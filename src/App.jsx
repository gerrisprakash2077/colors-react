import obj from './assets/data/colors.json'
import Color from './assets/components/Color'
let data = Object.entries(obj)

export default function App() {
    return (
        <div >
            {data.map((color) => {
                return <Color data={[...color]} key={color[0]} />
            })}
        </div>
    )
}
