import React from 'react'
import './variant.css'
let val = 100
export default function Variant(props) {
    let realVal = 50
    if (props.val >= 1) {
        realVal = props.val * 100;
    }
    return (
        <div >
            <div className="variant" style={{ backgroundColor: props.variant }}></div>
            <div className='variant-details'>

                <p className='val'>{realVal}</p>
                <p>{props.variant}</p>
            </div>
        </div>
    )
}
