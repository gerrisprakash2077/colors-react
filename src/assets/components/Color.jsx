import Variant from "./Variant"
import './colors.css'
export default function Color(props) {
    let color = props.data[0].split('')
    color[0] = color[0].toUpperCase()
    color = color.join('')

    return (
        <div className="all-colors">
            <div>
                <p className="color">{color}</p>
                <p className="color-detail">colors.{props.data[0]}</p>
            </div>
            <div className="colors">
                {props.data[1].map((variant, index) => {

                    return <Variant variant={variant} key={variant} val={index} />
                })}
            </div>
        </div>
    )
}
